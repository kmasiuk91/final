const recipeContainer = document.getElementById('recipe-container');
const today = document.getElementById('today');
const recipeEl = document.getElementById('recipe-search');
const ingrdntEl = document.getElementById('ingredient');
const inputError = document.getElementsByClassName('input-message')[0];

// clear display results before new display
const clearResults = () => {
    recipeContainer.innerHTML = '';
    today.innerHTML = '';
};

// errors dissapear 
let resetErrors = () => {
    ingrdntEl.classList.remove('is-invalid');
    ingrdntEl.classList.remove('is-valid');
    inputError.innerHTML = '';
}

// clean input after click
let resetFields = () => {
    ingrdntEl.value = '';
}

// checking validity of the input
const inputValid = () => {
    ingrdntEl.classList.remove('is-invalid');
    ingrdntEl.classList.add('is-valid');
    inputError.classList.remove('invisible');
    inputError.classList.add('visible')
    inputError.innerHTML = 'Sounds yammy!'
}

// checking invalidity of the input
const inputInvalid = () => {
    ingrdntEl.classList.remove('is-valid');
    ingrdntEl.classList.add('is-invalid');
    inputError.classList.remove('invisible');
    inputError.classList.add('visible')
    inputError.innerHTML = 'Bad food: type more letters'
}

// UNCOMMENT
// setTimeout(() => {
//     alert('Think faster!')
// }, 3000);


// in/validity run on input
const inputValidation = () => {
    if (ingrdntEl.value === "" || ingrdntEl.value.length < 3) {
        inputInvalid();
    } else {
        inputValid();
    }
    setTimeout(() => {
        resetErrors();
    }, 1500);
}
ingrdntEl.addEventListener('input', inputValidation);

// writing to local storage
const localStorageUse = () => {
    const toDo = 'You love to eat -->';
    localStorage.setItem(toDo, ingrdntEl.value);
    localStorage.getItem(ingrdntEl.value);
}

// cooking
recipeEl.addEventListener('submit', function(e) {
    e.preventDefault();

    // creating class to display searched food
    const ingrdntVal = ingrdntEl.value;
    class Ingredients {
        constructor(name) {
            this.name = name;
        }
    }
    let food = new Ingredients(ingrdntVal);

    // declaring a get url
    let url = `https://forkify-api.herokuapp.com/api/search?q=${ingrdntVal}`;

    fetch(url)
        .then(response => {
            console.log('API responding');
            return response.json();
        })
        .then(data => {
            const results = data.count;
            if (results > 0 ) {
                for (let i = 0; i < results; i++) {
                    recipeContainer.innerHTML += `
                        <div id="recipe-inner" class="card" style="width: 18rem;">
                            <img class="card-img-top" src="${data.recipes[i].image_url}" alt="${data.recipes[i].title} photo" />
                            <div class="card-body">
                                <h5 class="card-title">${data.recipes[i].title}</h5>
                                <p class="sub-header">By <a href="${data.recipes[i].publisher_url}" target="_blank" alt="${data.recipes[i].publisher}">${data.recipes[i].publisher}</a></p>
                                <a href="${data.recipes[i].source_url}" target="_blank" class="btn btn-dark btn-card">Full recipe</a>
                                <p class="sub-header">Recipe ID: ${data.recipes[i].recipe_id}</p>
                            </div>
                        </div>
                    `;
                };
                today.innerHTML += `
                <hr class="my-4">
                <h5>Today we are eating something with
                <span class="badge badge-success">${food.name}</span></h5>
                `;
            }
            else if (results === 0 || results === undefined) {
                inputInvalid(); // run input invalid error
                document.getElementById('recipe-container').innerHTML = `
                    <div class="card-body">
                        <h2><span class="badge badge-danger">Type correct food</span></h2>
                    </div>
                `;
                console.log('Type correct food!');
            }
        })
        .catch(err => {
          console.log(err);
        })
    localStorageUse(); // run writing to local storage
    clearResults(); // run clear display area
    resetFields(); // run clean inputs
}); 
