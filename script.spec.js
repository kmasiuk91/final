describe('Recipe App Test', () => {
    describe('inputValidation function', () => {
        it('checking if the input is valid', () => {
            const ingrdntEl = `<input id="ingredient" name="ingredient" class="form-control" type="text" placeholder="e.g. avocado" />`;
            let ingrdntVal = ingrdntEl.value;
            ingrdntVal = 'pizza';
            const result = true;
            if (ingrdntVal === '' || ingrdntVal.length < 3) {
                expect(result).toBe(false)
            } else {
                expect(result).toBe(true);
            }
        });
    });
    describe('API data request', () => {
        it('checking if the input text requests an recipe', () => {
            let results = 1;
            const result = true;
            if (results > 0) {
                expect(result).toEqual(true);
            } else if (results === 'undefined' || results === 0) {
                expect(result).toEqual(false);
            }
        });
    });
    describe('New display area creation', () => {
        it('should create new HTML markup', () => {
            const recipeContainer = document.getElementById('recipe-container');
            const result = true;
            if(recipeContainer != '') {
                expect(result).toBe(true);
            } else {
                expect(result).toBe(false);
            }
        });
    });
    describe('localStorageUse writing', () => {
        it('checking localStorageUse has been written to', () => {
            const ingrdntEl = `<input id="ingredient" name="ingredient" class="form-control" type="text" placeholder="e.g. avocado" />`;
            const ingrdntVal = ingrdntEl.value;
            const result = true;
            if (ingrdntVal != '') { // if ingrdntVal isn't empty -> expect true (write to local storage)
                expect(result).toBe(true);
            } else {
                expect(result).toBe(false)
            }
        });
    });
});